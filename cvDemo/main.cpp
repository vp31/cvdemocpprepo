#include <opencv2/opencv.hpp>
#include <iostream>

int main()
{
    std::string workDir = "/home/vinayak/imgs/";
    // Read the image file

    std::string rawFileName = "Lenna.png";
    std::string rawImgPath = workDir+rawFileName;
    cv::Mat img = cv::imread(rawImgPath);

    // Check for failure
    if (img.empty())
    {
        std::cout << "Could not open or find the image" << std::endl;
        std::cin.get(); //wait for any key press
        return -1;
    }

    std::string windowName = "Lenna Raw"; //Name of the window

    cv::namedWindow(windowName); // Create a window

    cv::imshow(windowName, img); // Show our image inside the created window.

    cv::waitKey(0); // Wait for any keystroke in the window

    cv::destroyWindow(windowName); //destroy the created window

    //release memory
    img.release();

    return 0;
}
